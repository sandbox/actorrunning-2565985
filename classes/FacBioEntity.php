<?php

class FacBioEntity {
  var $data;
  
  function __construct($data) {
    if ($this->verifyData($data)) $this->data = $data;
    else {
      throw new UnexpectedValueException;
    }
  }
  
  private function verifyData($data) {
    return is_array($data) && isset($data['url_ID']);
  }
}