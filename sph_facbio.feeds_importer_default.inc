<?php
/**
 * @file
 * sph_facbio.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function sph_facbio_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'facbio_appointments_importer';
  $feeds_importer->config = array(
    'name' => 'FacBio Appointments Importer',
    'description' => 'Imports Faculty Appointments from the SPH FacBio API.',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FacBioAppointmentsParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsFieldCollectionProcessor',
      'config' => array(
        'field_name' => 'field_faculty_appointment',
        'host_entity_type' => 'node',
        'is_field' => 1,
        'guid_field_name' => 'field_facbio_id',
        'identifier_field_name' => 'field_appointment_id',
        'mappings' => array(
          0 => array(
            'source' => 'job_class_ttl_web',
            'target' => 'field_appointment_title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'dept_ttl',
            'target' => 'field_department',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'url_ID',
            'target' => 'host_entity_guid',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'appt_ID',
            'target' => 'guid',
            'unique' => 1,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'field_faculty_appointment',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['facbio_appointments_importer'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'facbio_degrees_importer';
  $feeds_importer->config = array(
    'name' => 'FacBio Degrees Importer',
    'description' => 'Imports Faculty Degrees from the SPH FacBio API.',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FacBioDegreesParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsFieldCollectionProcessor',
      'config' => array(
        'field_name' => 'field_faculty_degrees',
        'host_entity_type' => 'node',
        'is_field' => 1,
        'guid_field_name' => 'field_facbio_id',
        'identifier_field_name' => 'field_appointment_id',
        'mappings' => array(
          0 => array(
            'source' => 'deg_ttl',
            'target' => 'field_degree',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'disc_ttl',
            'target' => 'field_degree_field',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'inst_ttl',
            'target' => 'field_degree_institution',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'yr_confrd',
            'target' => 'field_degree_year',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'url_ID',
            'target' => 'host_entity_guid',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'deg_ID',
            'target' => 'guid',
            'unique' => 1,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'field_faculty_degrees',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['facbio_degrees_importer'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'facbio_importer';
  $feeds_importer->config = array(
    'name' => 'FacBio Importer',
    'description' => 'Imports FacBio entities from the SPH FacBio API',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FacBioParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'FacBioProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'fname',
            'target' => 'field_first_name',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'mname',
            'target' => 'field_middle_name',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'lname',
            'target' => 'field_last_name',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'goes_by_name',
            'target' => 'field_goes_by_name',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'body_txt',
            'target' => 'body',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'email',
            'target' => 'field_location:email',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'phone',
            'target' => 'field_location:phone',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'office_num',
            'target' => 'field_location:name',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'fax',
            'target' => 'field_location:fax',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'street_addr',
            'target' => 'field_location:street',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'street_addr_2',
            'target' => 'field_location:additional',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'city',
            'target' => 'field_location:city',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'state',
            'target' => 'field_location:province',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'zip',
            'target' => 'field_location:postal_code',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'country',
            'target' => 'field_location:country',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'url_ID',
            'target' => 'guid',
            'unique' => 1,
          ),
          16 => array(
            'source' => 'url_ID',
            'target' => 'field_facbio_id',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'li',
            'target' => 'field_facbio_link',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'photo_URL',
            'target' => 'field_facbio_photo_url',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'ri_str',
            'target' => 'field_facbio_research_interests',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'URL',
            'target' => 'field_facbio_url',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 1,
        'bundle' => 'person',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['facbio_importer'] = $feeds_importer;

  return $export;
}