<?php

class FacBioParser extends FeedsParser {
  
  public function parse(FeedsSource $source, 
      FeedsFetcherResult $fetcher_result) {
    $string = $fetcher_result->getRaw();
    $arr = json_decode($string, TRUE); 
    
    foreach($arr as $k => $fac) {
      $arr[$k]['fac_type'] = $this->getFacultyTypes($fac['primary_appt_str']);
      
      // Research interests.
      if (isset($fac['ri'])) {
        foreach ($fac['ri'] as $interests) {
          if ($interests['fac_web_ttl'] == 'long') {
            $arr[$k]['body_txt'] = $interests['fac_web_text'];
          }        
        }
      }
      
      // Photo (url only - usually will point back to dept site, so largely
      // useless).
      if (isset($fac['ph'])) {
        foreach ($fac['ph'] as $ph) {
          if ($ph['photo_URL']) {
            $arr[$k]['photo_URL'] = $ph['photo_URL'];
          }          
        }
      }
      
      // Links (often links to dept assets - again, largely useless).
      if (isset($fac['li'])) {
        foreach ($fac['li'] as $li) {
          $primary_link = array_shift($fac['li']);
          if (isset($primary_link['fac_web_text'])) {
            $arr[$k]['li'] = strtolower($primary_link['fac_web_text']);
          }
        }
      }
      
      // Contact info.
      if (isset($fac['co'])) {
        if (isset($fac['co'])) {
          $arr[$k] += $fac['co'];
        }      
        $arr[$k]['phone'] = $fac['co']['phone_area'] . '-' . 
            $fac['co']['phone_pre'] . '-' . $fac['co']['phone_suf'];
        $arr[$k]['phone2'] = $fac['co']['phone_area_2'] . '-' . 
            $fac['co']['phone_pre_2'] . '-' . $fac['co']['phone_suf_2'];
        $arr[$k]['fax'] = $fac['co']['fax_area'] . '-' . 
            $fac['co']['fax_pre'] . '-' . $fac['co']['fax_suf'];
        if (preg_replace('/\s+/', '', $arr[$k]['phone']) == '--') unset($arr[$k]['phone']);
        if (preg_replace('/\s+/', '', $arr[$k]['phone2']) == '--') unset($arr[$k]['phone2']);
        if (preg_replace('/\s+/', '', $arr[$k]['fax']) == '--') unset($arr[$k]['fax']);
        
        // Consolidate street_addr_2 and po_box fields to fit in Location.
        if (isset($arr[$k]['po_box'])) {
          if (isset($arr[$k]['street_addr_2'])) {
            $arr[$k]['street_addr_2'] .= ' ' . $arr[$k]['po_box'];
          }
          else {
            $arr[$k]['street_addr_2'] = $arr[$k]['po_box'];
          }
        }
      }
      
      // URL (will be "http://" if empty).      
      if (isset($fac['co']['URL']) && $fac['co']['URL'] == 'http://') {
        unset($arr[$k]['URL']);
      }
    }
    $result = new FeedsParserResult($arr);
    $result->title = 'What\'s the title for?'; 
    return $result;
  }
  
  public function getMappingSources() {
    return array(
      'url_ID' => array(
        'name' => t('URL ID'),
        'description' => t('Unique URL ID.'),
      ),
      'fname' => array(
        'name' => t('First Name'),
      ),
      'mname' => array(
        'name' => t('Middle Name'),
      ),
      'lname' => array(
        'name' => t('Last Name'),
      ),
      'goes_by_name' => array(
        'name' => t('Goes By Name'),
      ),
      'ri_str' => array(
        'name' => t('Research Interests'),
      ),
      'body_txt' => array(
        'name' => t('Bio'),
      ),
      'photo_URL' => array(
        'name' => t('Photo URL'),
      ),
      'li' => array(
        'name' => t('Link'),
      ),
      'email' => array(
        'name' => t('Email'),
      ),
      'phone' => array(
        'name' => t('Phone'),
      ),
      'phone2' => array(
        'name' => t('Phone 2'),
      ),
      'fax' => array(
        'name' => t('Fax'),
      ),
      'office_num' => array(
        'name' => t('Office Number'),
      ),
      'street_addr' => array(
        'name' => t('Address 1'),
      ),
      'street_addr_2' => array(
        'name' => t('Address 2'),
      ),
      'street_addr_3' => array(
        'name' => t('Address 3'),
      ),
      'city' => array(
        'name' => t('City'),
      ),
      'state' => array(
        'name' => t('State'),
      ),
      'country' => array(
        'name' => t('Country'),
      ),
      'zip' => array(
        'name' => t('Zip'),
      ),
      'URL' => array(
        'name' => t('URL'),
      ),
    );        
  }
  
  private function getFacultyTypes($primary_appt_str) {
    $primary_appt_str = strtolower(preg_replace("/[^[:alnum:][:space:]]/ui", 
        '', $primary_appt_str));
    $title = explode(' ', $primary_appt_str);
    if (!in_array('biostatistics', $title)) return NULL;
    
    $types = array(
      'adjunct', 'affiliate', 'emeritus', 'visiting', 'fellow', 'research',
    );
    $class = array(
      'professor and chair biostatistics', 'professor biostatistics', 
      'associate professor bistatistics', 'assistant professor biostatistics',
    );
    
    $return = array();
    foreach($types as $type) {
      if (in_array($type, $title)) {
        $return[] = ($type == 'fellow') ? 'fellows' : $type;
      }
    }
    if (in_array($primary_appt_str, $class)) {
      $return[] = 'core';
    }
    
    return $return;
  }
}

class FacBioAppointmentsParser extends FeedsParser {
  
  public function parse(FeedsSource $source, 
      FeedsFetcherResult $fetcher_result) {
    $string = $fetcher_result->getRaw();
    $arr = json_decode($string, TRUE); 
    
    // Extract only the url_ID (as unique id) and Appointments information.
    $output = array(); $i = 0;
    foreach($arr as $k => $a) {      
      $url_ID = $a['url_ID'];
      if (isset($a['appt'])) {              
        foreach($a['appt'] as $item) {
          $output[$i] = $item;
          $output[$i]['appt_ID'] = $url_ID . '-' . 
              $item['job_class_ttl_web'] . '-' . $item['dept_ttl'];
          $output[$i++]['url_ID'] = $url_ID;
        }
      }
    }
    $result = new FeedsParserResult($output);
    $result->title = 'What\'s the title for?';
    return $result;
  }
  
  public function getMappingSources() {
    return array(
      'job_class_ttl_web' => array(
        'name' => t('Appointment Title'),
      ),
      'dept_ttl' => array(
        'name' => t('Department'),
      ),
      'url_ID' => array(
        'name' => t('FacBio ID'),
        'description' => t('Unique FacBio ID'),
      ),
      'appt_ID' => array(
        'name' => t('Appointment ID'),
        'description' => t('Unique Appointment ID'),
      ),
    );        
  }  
}

class FacBioDegreesParser extends FeedsParser {
  
  public function parse(FeedsSource $source, 
      FeedsFetcherResult $fetcher_result) {
    $string = $fetcher_result->getRaw();
    $arr = json_decode($string, TRUE); 
    
    // Extract only the url_ID (as unique id) and Appointments information.
    $output = array(); $i = 0;
    foreach($arr as $k => $a) {      
      $url_ID = $a['url_ID'];
      if (isset($a['de'])) {
        foreach($a['de'] as $item) {
          $output[$i] = $item;
          $output[$i]['deg_ID'] = $url_ID . '-' . $item['deg_ttl'] . '-' 
              . $item['disc_ID'] . '-' . $item['yr_confrd'];
          $output[$i++]['url_ID'] = $url_ID;
        }
      }
    }
    $result = new FeedsParserResult($output);
    $result->title = 'FacBio Degrees Parser Result';
    return $result;
  }
  
  public function getMappingSources() {
    return array(
      'deg_ttl' => array(
        'name' => t('Degree Title'),
      ),
      'disc_ttl' => array(
        'name' => t('Field'),
      ),
      'inst_ttl' => array(
        'name' => t('Insitution'),
      ),
      'yr_confrd' => array(
        'name' => t('Year'),
      ),
      'url_ID' => array(
        'name' => t('FacBio ID'),
        'description' => t('Unique FacBio ID'),
      ),
      'deg_ID' => array(
        'name' => t('Degree ID'),
        'description' => t('Unique Degree ID'),
      ),
    );        
  }  
}