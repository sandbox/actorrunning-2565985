<?php 

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

class DataAccess {
  var $dept_ID;
  /*var $keys = array(
        'publish' => '_get_publish_num', 'ph' => '_get_ph_array', 
        'ri' => '_get_ri_array', 'pa' => '_get_pa_array', 
        'de' => '_get_de_array', 'li' => '_get_li_array', 
        'co' => '_get_co_array', 'name' => '_get_name_array', 
        'appt' => '_get_appt_array', 'appt_str' => '_get_appt_str_array', 
        'ri_keywords' => '_get_ri_keywords_array', 'sv' => '_get_sv_array',
      );*/
  var $keys = array(
        'publish' => '_get_publish_num', 'ph' => '_get_ph_array', 
        'ri' => '_get_ri_array',
        'de' => '_get_de_array', 'li' => '_get_li_array', 
        'co' => '_get_co_array',  
        'appt' => '_get_appt_array',  
        'sv' => '_get_sv_array',
      );
  var $mult_counts = array('ph' => 0, 'pa' => 0, 'ri' => 0, 'de' => 0, 'li' => 0, 'appt' => 0);
  
  function __construct($dept_ID) {
    $this->dept_ID = $dept_ID;
  }
  
  function getAllBios() {    
    $xml = $this->_load_bios_by_dept();
    
    $bios = array();
    foreach($xml->a as $facBio) {
      foreach ($facBio as $pname => $pval) {
        $bios[(string) $facBio->url_ID][$pname] = (string) $pval;
      }
        
      $xml = $this->_load_bio_by_url_id($facBio->url_ID);
      $this->_reset_mult_counts();
      $index = (string) $facBio->url_ID; 
      foreach($xml as $key => $field) {
        if (in_array($key, array_keys($this->keys))) {
          if (in_array($key, array_keys($this->mult_counts))) {
            $bios[$index][$key][$this->mult_counts[$key]++] = $this->{$this->keys[$key]}($field);
          }
          else {
            $bios[$index][$key] = $this->{$this->keys[$key]}($field);
          }
        }
      }
    }
    return $bios;
  }
  
  function getUpdatedBios() {
    
  }
  
  private function _reset_mult_counts() {
    foreach ($this->mult_counts as $k => $v) {
      $this->mult_counts[$k] = 0;
    }
  }
  
  private function _get_publish_num($field) {
    return (string) $field->publish_num;
  }
  
  private function _get_ri_array($field) {
    return array(
      'fac_web_ttl' => (string) $field->fac_web_ttl,
      'fac_web_text' => (string) $field->fac_web_text,
      'fac_web_publ_seq_num' => (string) $field->fac_web_publ_seq_num,
    );
  }
  
  private function _get_pa_array($field) {
    return array(
      'fac_web_text' => (string) $field->fac_web_text,
      'rel_seq_num' => (string) $field->rel_seq_num,
      'fac_web_publ_seq_num' => (string) $field->b->fac_web_publ_seq_num,
      'prog_ID' => (string) $field->b->c->prog_ID,
      'prog_ttl' => (string) $field->b->c->prog_ttl,
      'prog_URL' => (string) $field->b->c->prog_URL,
    );
  }
  
  private function _get_ph_array($field) {
    return array(
      'fac_web_text' => (string) $field->fac_web_text,
      'rel_seq_num' => (string) $field->rel_seq_num,
      'fac_web_publ_seq_num' => (string) $field->b->fac_web_publ_seq_num,
      'photo_URL' => (string) $field->b->c->photo_URL,
      'photo_width' => (string) $field->b->c->photo_width,
      'photo_height' => (string) $field->b->c->photo_height,
    );
  }
  
  private function _get_de_array($field) {
    return array(
      'fac_web_text' => (string) $field->fac_web_text,
      'reg_seq_num' => (string) $field->rel_seq_num,
      'fac_web_publ_seq_num' => (string) $field->b->c->fac_web_publ_seq_num,
      'deg_ID' => (string) $field->b->c->deg_ID,
      'disc_ID' => (string) $field->b->c->disc_ID,
      'disc_ttl' => (string) $field->b->c->disc_ttl,
      'yr_confrd' => (string) $field->b->c->yr_confrd,
      'deg_ttl' => (string) $field->b->c->d->deg_ttl,
      'schl_ID' => (string) $field->b->c->d->f->schl_ID,
      'inst_ID' => (string) $field->b->c->d->f->inst_ID,
      'inst_ttl' => (string) $field->b->c->d->f->g->inst_ttl,
      'schl_ttl' => (string) $field->b->c->d->f->g->h->schl_ttl,
    );
  }
  
  private function _get_li_array($field) {
    return array(
      'fac_web_ttl' => (string) $field->fac_web_ttl,
      'fac_web_text' => (string) $field->fac_web_text,
      'fac_web_publ_seq_num' => (string) $field->fac_web_publ_seq_num,
    );
  }
  
  private function _get_co_array($field) {
    return array(
      'fac_web_text' => (string) $field->fac_web_text,
      'rel_seq_num' => (string) $field->rel_seq_num,
      'fac_web_publ_seq_num' => (string) $field->b->fac_web_publ_seq_num,
      'active' => (string) $field->b->c->active,
      'preferred' => (string) $field->b->c->preferred,
      'email' => (string) $field->b->c->email,
      'phone_area' => (string) $field->b->c->phone_area,
      'phone_pre' => (string) $field->b->c->phone_pre,
      'phone_suf' => (string) $field->b->c->phone_suf,
      'phone_area_2' => (string) $field->b->c->phone_area_2,
      'phone_pre_2' => (string) $field->b->c->phone_pre_2,
      'phone_suf_2' => (string) $field->b->c->phone_suf_2,
      'fax_area' => (string) $field->b->c->fax_area,
      'fax_pre' => (string) $field->b->c->fax_pre,
      'fax_suf' => (string) $field->b->c->fax_suf,
      'office_num' => (string) $field->b->c->office_num,
      'po_box' => (string) $field->b->c->po_box,
      'street_addr' => (string) $field->b->c->street_addr,
      'street_addr_2' => (string) $field->b->c->street_addr_2,
      'street_addr_3' => (string) $field->b->c->street_addr_3,
      'city' => (string) $field->b->c->city,
      'state' => (string) $field->b->c->state,
      'country' => (string) $field->b->c->country,
      'zip' => (string) $field->b->c->zip,
      'zip_4' => (string) $field->b->c->zip_4,
      'con_typ_ID' => (string) $field->b->c->con_typ_ID,
      'URL' => (string) $field->b->c->URL,
      'inst_ID' => (string) $field->b->c->inst_ID,
      'inst_ttl' => (string) $field->b->c->d->inst_ttl,
      'cntry_ttl' => (string) $field->b->c->d->e->cntry_ttl,
    );
  }
  
  private function _get_name_array($field) {
    return array(
      'fname' => (string) $field->fname,
      'mname' => (string) $field->mname,
      'lname' => (string) $field->lname,
      'goes_by_name' => (string) $field->goes_by_name,
    );
  }
  
  private function _get_appt_array($field) {
    return array(
      'job_class_ttl_web' => (string) $field->job_class_ttl_web,
      'dept_ttl' => (string) $field->dept_ttl,
      'joint_ind' => (string) $field->joint_ind,
      'home' => (string) $field->home,
    );
  }
  
  private function _get_appt_str_array($field) {
    return array(
      'appt_str' => (string) $field->job_class_ttl_str,
    );
  }
  
  private function _get_ri_keywords_array($field) {
    return array(
      'keyword_string' => (string) $field->keyword_string,
    );
  }
  
  private function _get_sv_array($field) {
    return array(
      'scivalID' => (string) $field->scivalID,
    );
  }
  
  private function _load_bios_by_dept() {
    $url = "http://apps.sphcm.washington.edu/xml/pub/facDept.xml?dept_ID=$this->dept_ID&reg=true&res=true&adj=true&cli=true&aff=true&non=true";
    return simplexml_load_string($this->_make_api_call($url));
  }
  
  private function _load_bio_by_url_id($url_ID) {
    $url = "http://apps.sphcm.washington.edu/xml/pub/facBio.xml?url_ID=$url_ID&web_publ_ID=$this->dept_ID";
    return simplexml_load_string($this->_make_api_call($url));
  }
  
  private function _make_api_call($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    $output = curl_exec($ch);
    curl_close($ch);
    
    return $output;
  }
}