<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('DataAccess.php');
require_once('FacBioEntity.php');

class FacBioController {
  var $DA;
  var $DM;
  var $FacBioEntityQueue;
  
  function __construct($dept_ID) {
    $this->DA = new DataAccess($dept_ID);
    $this->FacBioEntityQueue = array();
  }
  
  function getNewBios($all = FALSE) {
    $data_array = ($all) ? $this->DA->getAllBios() : $this->DA->getUpdatedBios();    
    foreach ($data_array as $data) {
      $this->FacBioEntityQueue[] = new FacBioEntity($data);
    }
  }  
}