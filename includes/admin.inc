<?php

/**
 * Implements hook_form().
 * @param $form
 * @return array
 */
function sph_facbio_admin_form($form) {
  $dept_id = variable_get('sph_facbio_dept_id', '');
//  $update_interval = variable_get('sph_facbio_update_interval', '');
  
  $form['sph_facbio_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
  );
  $form['sph_facbio_config']['dept_id'] = array(
    '#type' => 'textfield', 
    '#title' => t('Department ID'),
    '#description' => t('The Department ID in the SPH FacBio System: biostat '
        . '(Biostatistics), envhlth (DEOHS), epi (Epidemiology), glbhlth '
        . '(Global Health), hserv (Health Services)'),
    '#size' => 8,
    '#default_value' => $dept_id,
  ); 
//  $form['sph_facbio_config']['update_interval'] = array(
//    '#type' => 'select',
//    '#title' => t('Update Interval'),
//    '#description' => t('How often would you like to automatically update the '
//        . 'SPH FacBio info on the site? (Note: this runs on cron, and cannot '
//        . 'be more granular than your cron run interval, i.e., if you have '
//        . 'cron set to run every day, and this set to run every hour, it will '
//        . 'only run every day.'),
//    '#options' => array(
//      'never' => t('Never'),
//      '+1 hour' => t('1 hour'),
//      '+3 hours' => t('3 hours'),
//      '+6 hours' => t('6 hours'),
//      '+12 hours' => t('12 hours'),
//      '+1 day' => t('1 day'),
//      '+1 week' => t('1 week'),
//    ),  
//    '#default_value' => $update_interval,
//  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );  
  
  return $form;
}

/**
 * Custom submit handler for the sph_facbio_admin_form.
 * @param $form
 * @param $form_state
 */
function sph_facbio_admin_form_submit($form, &$form_state) { 
  if ($form_state['clicked_button']['#type'] == 'submit') {
    variable_set('sph_facbio_dept_id', $form_state['values']['dept_id']);
//    variable_set('sph_facbio_update_interval', 
//        $form_state['values']['update_interval']);
  }
}