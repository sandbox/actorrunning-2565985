-- SUMMARY --

The SPH FacBio module creates a one-way integration between the UW SPH FacBio 
database API and a Drupal site. Because the SPH FacBio API is a read-only
data source, this integration is limited to read only.

The module provides a Person content type, and a Person Type taxonomy, bringing
faculty members imported into the site as content of type Person, and of Person
Type Faculty. This structure is provided with features, so feel free to extend
it to fit your needs.

Since this is one-way, we have incorporated a way for sites to override the
data coming from FacBio and display their own.

The project page for the module can be found here:
  http://drupal.org/project/uw_sph_facbio


-- REQUIREMENTS --

None (other than a multitude of module dependencies, noted in the .info file).


-- INSTALLATION -- 

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure the module on it's config page: /admin/config/content/uw_sph_facbio

  - Here you can set the deptartment id, which will pull the correct
    department faculty members

* To manually pull all faculty: 

  - Go to /import/facbio_importer 
  - Ensure that the base url in the URL field is the base URL of your site
  - Click "Import"


-- USAGE --

* Periodic imports will take place with every cron run.

* Content will be created automatically. Since the data for the content is
   coming from a remote source, the content is not editable at the outset.

* To change the values displayed for a particular faculty member: 
  
  - Edit the content you wish to change.
  - Click "Override SPH FacBio"
  - Edit and Save

* To revert to the original FacBio defaults:
 
  - Unclick "Override SPH FacBio" - on the next import the data will be 
    overwritten