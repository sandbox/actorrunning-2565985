<?php
require_once('classes/FacBioController.php');
$dept_id = $_REQUEST['dept_id'];

$C = new FacBioController($dept_id);
$C->getNewBios(TRUE);

$bios = array();
foreach ($C->FacBioEntityQueue as $fb) {
  $bios[] = $fb->data;
}

header('Content-Type: text/plain');
echo json_encode($bios, JSON_PRETTY_PRINT);